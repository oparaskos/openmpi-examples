/*
 * MPI Ping-Pong example using OpenMPI C++ API
 * - `mpic++ this_file.cpp -o out_file` to compile
 * - `mpirun -n 2 this_file` to run it (world size must be 2)
 */

#include <mpi.h>
#include <iostream>

int main(int argc, char** argv) {
  const int PING_PONG_LIMIT = 10;
  // Init and Finalise belong to the namespace MPI
  MPI::Init(/* Optionally accept argc, argv */);
  // Get_rank and Get_size are methods of the Comm class (however they are virtual in there)
  //  MPI::COMM_WORLD is a global instance of Comm
  int world_rank = MPI::COMM_WORLD.Get_rank();
  int world_size = MPI::COMM_WORLD.Get_size();
  if(world_size != 2) {
    std::cerr << "World size must be 2 for " << argv[0] << std::endl;
    MPI::COMM_WORLD.Abort(1);
  }
  std::cout << "Started " << argv[0] << " [" << world_rank << ']' << std::endl;
  int ping_pong_count = 0;
  int partner_rank = (world_rank + 1) % world_size;
  while(ping_pong_count < PING_PONG_LIMIT) {
    if(world_rank == ping_pong_count % world_size) {
      ++ping_pong_count;
      // No need to add MPI_COMM_WORLD at the end of these as it is known already
      //  There is an MPI::INT though as its an enum both ways should work
      MPI::COMM_WORLD.Send(&ping_pong_count, 1, MPI::INT, partner_rank, 0);
      std::cout << "ping\n";
    } else {
    	// There is an optional status parameter to this of type MPI::Status& 
      MPI::COMM_WORLD.Recv(&ping_pong_count, 1, MPI::INT, partner_rank, 0);
      std::cout << "pong\n";
    }
  }
  MPI::Finalize();
}

