#include <iostream> // std::cout
#include <algorithm>// std::transform
#include <iterator> // std::begin
#include <chrono>   // std::chrono::system_clock
#include <mpi.h>    // MPI::

enum { 
  NUM_ELEMENTS_PER_EACH = 5,
};

int main(int argc, char** argv) {
  namespace system_clock = std::chrono::system_clock;
  // Initialise MPI
  MPI::Init();
  int world_rank = MPI::COMM_WORLD.Get_rank();
  // Use time and rank as seed values
  unsigned long time = system_clock::now().time_since_epoch().count();
  // Add some numbers together
  std::default_random_engine generator(time + world_rank);
  std::uniform_real_distribution<float> distribution(1, 200);
  // Get some random numbers to sum
  float some_nums[NUM_ELEMENTS_PER_EACH];
  std::transform(std::begin(some_nums), std::end(some_nums), std::begin(some_nums),
		 [&](float a) -> float { return distribution(generator); });
  // Sum the numbers locally
  float local_sum = std::accumulate(std::begin(some_nums), std::end(some_nums), 0);
  // Print the random numbers on each process
  //std::cout << "Local sum for process " << world_rank 
  //	    << " - " << local_sum << std::endl;
  // Reduce all of the local sums into the global sum
  float global_sum;
  MPI::COMM_WORLD.Reduce(&local_sum, &global_sum, 1, MPI_FLOAT, MPI_SUM, 0);
  // Print the result
  if (world_rank == 0)
    std::cout << "Total = " << global_sum << std::endl;
  MPI::Finalize();
}
